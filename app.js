const autobahn = require('autobahn');

const hostname = 'mercury.beta.c3wireless.com';
const port = 8080;

let connection = new autobahn.Connection({
    url: `ws://${hostname}:${port}/ws`,
    realm: 'realm1'
});

connection.onopen = (session) => {
    console.log('Opened session');
    const on_listener_list = (args) => {
        const evt = args[0];
        if (evt.event_type !== 0) {
            return;
        }
        for (let listener_id of evt.data) {
            session.call(`com.c3wireless.listeners.${listener_id}.rpc.ping`).then(() => {
                console.log(`\t${listener_id}: Up`);
            }, (e) => {
                console.log(`\t${listener_id}: Down (${e})`);
            });
        }
    };
    session.subscribe('com.c3wireless.aggregators..events', on_listener_list, {match: 'wildcard'});

    const get_listener_list = () => {
        const date = new Date();
        console.log(`Listener status at ${date}:`);
        session.publish('com.c3wireless.aggregators', [{'event_type': 0}]);
    };
    get_listener_list();
    setInterval(get_listener_list, 5000);
};

console.log(`Trying to connect to ws://${hostname}:${port}/ws`);
connection.open();