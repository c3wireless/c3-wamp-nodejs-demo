# C3 Aggregator Health Check

This test application connects to the wamp router, and periodically queries the aggregators 
for connected readers and pings them to ensure that they are online.

## Install

1. Clone repo
2. `npm install && node app.js`

## Configuration

WAMP router hostname and port is hard-coded in the source file.